import { Container } from "./components/Container/Container";
import { Heading } from "./components/Heading/Heading";
import { ServicesList } from "./components/ServicesList/ServicesList";
import { Services } from "./sections/Services/Services";

function App() {
  return (
    <div className="App">
      <Container>
        <Services />
        <ServicesList />
        <Heading sub="Our Portofolio" main="Take A Look At Our Latest Work" />
      </Container>
    </div>
  );
}

export default App;
