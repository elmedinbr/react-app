import React from "react";
import { Heading } from "../../components/Heading/Heading";

import "./Services.css";
export const Services = () => {
  return (
    <div className="Services">
      <Heading
        center
        sub="Our Service"
        main="We Are Providing Digital services"
      />
    </div>
  );
};
