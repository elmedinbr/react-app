import React from "react";

import "./Heading.css";
export const Heading = (props) => {
  return (
    <div className={"Heading" + (props.center ? " Heading--center " : "")}>
      <h4 className="Heading__sub-title">{props.sub}</h4>
      <h2 className="Heading__main-title">{props.main}</h2>
    </div>
  );
};
