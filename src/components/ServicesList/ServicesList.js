import React from "react";

import CircleImage from "..//..//assets/images/CircleImage.svg";
import "./ServicesList.css";
export const ServicesList = () => {
  return (
    <div className="ServicesList">
      <div className="ServicesList-item">
        {" "}
        <div className="SericesList__image">
          <img src={CircleImage} />
        </div>
        <h4 className="ServicesList__title">Web Development</h4>
        <p className="ServicesList">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmodtempor incididunt ut labore et dolore
        </p>
      </div>
      <div className="ServicesList-item">
        <div className="SericesList__image">
          <img src={CircleImage} />
        </div>
        <h4 className="ServicesList__title">Web Development</h4>
        <p className="ServicesList">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmodtempor incididunt ut labore et dolore
        </p>
      </div>
      <div className="ServicesList-item">
        <div className="SericesList__image">
          <img src={CircleImage} />
        </div>
        <h4 className="ServicesList__title">Web Development</h4>
        <p className="ServicesList">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmodtempor incididunt ut labore et dolore
        </p>
      </div>
    </div>
  );
};
